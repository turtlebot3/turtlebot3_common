turtlebot3_SBC_Setting
===

두 가지 방법이 있는데

1. Ubuntu MATE
2. Raspbian

우선은 Ubuntu MATE로 설치를 해본다.

> (참고) Ubuntu MATE도 용량이 크기에 최소 16GB이상이여야 함.

---

Ubuntu MATE : 16.04.5(Xenial)

[다운로드 사이트](https://ubuntu-mate.org/download/)<br>

> (참고로 NAS 서버에도 있음)

---

Turtlebot3 PC에서 SBC의 Ubuntu MATE를 다음과 같이 올린다.

참고로 나는 Turtlebot3 PC가 VirtualBox에 설치해서 설정이 좀 더 들어갔음.(상세설정은 쉽기에 생략)

![이미지](/image/SBC_Setting_image1.png)<br>

![이미지2](/image/SBC_Setting_image2.png)<br>

목록 -> 디스크 이미지 복구 선택

안될경우는 이미지파일 손상의 확률이 크기에 이미지파일을 다시 다운 받는 것을 추천

위가 안될경우(VirtualBox는 안됨) [여기](https://yeopbox.com/%EB%9D%BC%EC%A6%88%EB%B2%A0%EB%A6%AC%ED%8C%8C%EC%9D%B4-3-raspberry-pi%EC%97%90-%EC%9A%B0%EB%B6%84%ED%88%AC-%EB%A9%94%EC%9D%B4%ED%8A%B8-16-04-lts-%EC%84%A4%EC%B9%98%ED%95%98%EA%B8%B0/)<br>를 참고

차이점은 SDCard 포매터로 포맷 이후 Win32 Disk Imager로 Ubuntu MATE를 설치한다는 점.

[SDCard 포매터](https://www.sdcard.org/downloads/formatter_4/)<br>
[Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/files/latest/download)<br>

`결론은 윈도우에서 SDCard Formatter + Win32 Disk Imager 조합이 나음 리눅스보다`

---

[SBC]

> 설치가 오래걸리기에 시간을 넉넉히 준비해야한다.

라즈베리파이에 HDMI케이블, 마우스, 키보드 연결하여 Ubuntu MATE 설치

설치는 일반 우분투 리눅스 설치와 동일하기에 생략.

```c

# 설치완료 후 아래 명령어 입력
$ sudo apt-get update
$ sudo apt-get upgrade
$ wget https://raw.githubusercontent.com/ROBOTIS-GIT/robotis_tools/master/install_ros_kinetic_rp3.sh && chmod 755 ./install_ros_kinetic_rp3.sh && bash ./install_ros_kinetic_rp3.sh

```

```c

# 필요한 SW 클론
$ git clone https://github.com/ROBOTIS-GIT/hls_lfcd_lds_driver.git
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3.git

```

```c

# 필요없는 SW제거
$ cd ~/catkin_ws/src/turtlebot3
$ sudo rm -r turtlebot3_description/ turtlebot3_teleop/ turtlebot3_navigation/ turtlebot3_slam/ turtlebot3_example/

```

```c

# 필요한 패키지 설치
$ sudo apt-get install ros-kinetic-rosserial-python ros-kinetic-tf

```

```c

# USB 세팅
$ rosrun turtlebot3_bringup create_udev_rules

```

아이피 주소 세팅

![이미지3](/image/SBC_Setting_image3.PNG)

```c

$ sudo gedit ~/.bashrc

```

![이미지4](/image/SBC_Setting_image4.PNG)

```c

$ source ~/.bashrc

```
---

## PC에서 SBC로 SSH로 접근하기

SBC에 SSH를 설치해야함.

```c

$ dpkg --get-seletions | grep ssh
# ssh 설치여부를 출력

```

```c

$ service ssh status
# 현재 ssh가 실행중인지 여부를 출력

```

```c

$ service ssh restart
# 설치 후 재시작

```

```c

# 설치를 했는대 inactive라면
$ sudo nano /etc/rc.local

# exit 0 위에 다음을 추가한다.
$ /bin/sh -e /etc/init.d/ssh start

```

[참고사이트1](https://zetawiki.com/wiki/%EC%9A%B0%EB%B6%84%ED%88%AC_sshd_%EC%84%A4%EC%B9%98)<br>
[참고사이트2](http://ocworld.net/?p=2304)<br>

---

[참고사이트](http://emanual.robotis.com/docs/en/platform/turtlebot3/sbc_setup/)<br>
