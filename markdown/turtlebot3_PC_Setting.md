Turtlebot3 PC Setting
===

# Turtlebot3 개발환경 구축

ubuntu-16.04.4-desktop-amd64


## ROS 설치

```c

$ sudo apt-get update
$ sudo apt-get upgrade
$ wget https://raw.githubusercontent.com/ROBOTIS-GIT/robotis_tools/master/install_ros_kinetic.sh && chmod 755 ./install_ros_kinetic.sh && bash ./install_ros_kinetic.sh

```

## Turtlebot3 관련 패키지 설치

```c

$ sudo apt-get install ros-kinetic-joy ros-kinetic-teleop-twist-joy ros-kinetic-teleop-twist-keyboard ros-kinetic-laser-proc ros-kinetic-rgbd-launch ros-kinetic-depthimage-to-laserscan ros-kinetic-rosserial-arduino ros-kinetic-rosserial-python ros-kinetic-rosserial-server ros-kinetic-rosserial-client ros-kinetic-rosserial-msgs ros-kinetic-amcl ros-kinetic-map-server ros-kinetic-move-base ros-kinetic-urdf ros-kinetic-xacro ros-kinetic-compressed-image-transport ros-kinetic-rqt-image-view ros-kinetic-gmapping ros-kinetic-navigation ros-kinetic-interactive-markers

```

## Network Setting

가상머신을 사용한다면 어댑터에 브리지로 설정

![이미지1](/image/common_image_01.PNG)<br>

브리지로 설정할 경우 내부방에 IP가 할당됨을 확인 가능

![이미지2](/image/common_image_02.PNG)<br>


IP_OF_REMOTE_PC : 현재 PC의 아이피로

IP_OF_TURTLEBOT : 내부망으로 하고 임의로 해도 좋음

![이미지3](/image/common_image_03.PNG)<br>

```c

# ROS_MASTER_URI
# ROS_HOSTNAME
# 을 수정한다.
$ sudo gedit ~/.bashrc

```

![이미지4](/image/common_image_04.PNG)<br>

```c

# 수정이후 다음 커맨드
$ source ~/.bashrc

```

---

[참고사이트](http://emanual.robotis.com/docs/en/platform/turtlebot3/pc_setup/)<br>