ROS - Turtlebot3
===

### Main

* 01 개발환경 구축
    * [01.1 PC세팅](#01.1PC세팅)<br>
    * [01.2 라즈베리파이 세팅](#01.2라즈베리파이세팅)<br>
    * 미작성 : [01.3 OpenCR 세팅](#01.3OpenCR세팅)<br>
* 02 Bring up
    * [02. Bring up](#02.Bringup)<br>
    * [02.1 Topic Monitor](#02.1_Topic_Monitor_실행)<br>
* 03 SLAM
    * [03.1 SLAM](#03.1SLAM)<br>
* 00 참고자료
    * [ROS launch 설명](#ROS_launch_설명)<br>
        * [turtlebot3_robot.launch file](#turtlebot3_robot.launch)<br>
        * [turtlebot3_core.launch file](#turtlebot3_core.launch)<br>
        * [turtlebot3_lidar.launch file](#turtlebot3_lidar.launch)<br>
    * [ROBOTIS Git 홈페이지](https://github.com/ROBOTIS-GIT)<br>
    * [ttyACM, ttyUSB 정의](markdown/ttyACMUSB)<br>
    * [리눅스 rules](markdown/rules)<br>
    * [rosserial_python](#rosserial_python)<br>
* 00 메시지 정의
    * [diagnostic_msgs WiKi](http://wiki.ros.org/diagnostic_msgs)<br>
    * [common msgs Github](https://github.com/ros/common_msgs)<br>
    * [hls_lfcd_lds_driver Github](https://github.com/ROBOTIS-GIT/hls_lfcd_lds_driver)<br>

---

[Top](#Main)<br>

# 01.1PC세팅

## Ubuntu 환경

ubuntu-16.04.4-desktop-amd64


## ROS 설치

```c

$ sudo apt-get update
$ sudo apt-get upgrade
$ wget https://raw.githubusercontent.com/ROBOTIS-GIT/robotis_tools/master/install_ros_kinetic.sh && chmod 755 ./install_ros_kinetic.sh && bash ./install_ros_kinetic.sh

```

## Turtlebot3 관련 패키지 설치

```c

$ sudo apt-get install ros-kinetic-joy ros-kinetic-teleop-twist-joy ros-kinetic-teleop-twist-keyboard ros-kinetic-laser-proc ros-kinetic-rgbd-launch ros-kinetic-depthimage-to-laserscan ros-kinetic-rosserial-arduino ros-kinetic-rosserial-python ros-kinetic-rosserial-server ros-kinetic-rosserial-client ros-kinetic-rosserial-msgs ros-kinetic-amcl ros-kinetic-map-server ros-kinetic-move-base ros-kinetic-urdf ros-kinetic-xacro ros-kinetic-compressed-image-transport ros-kinetic-rqt-image-view ros-kinetic-gmapping ros-kinetic-navigation ros-kinetic-interactive-markers

```

## Network Setting

가상머신을 사용한다면 어댑터에 브리지로 설정

![이미지1](/image/common_image_01.PNG)<br>

브리지로 설정할 경우 내부방에 IP가 할당됨을 확인 가능

![이미지2](/image/common_image_02.PNG)<br>


IP_OF_REMOTE_PC : 현재 PC의 아이피로

IP_OF_TURTLEBOT : 내부망으로 하고 임의로 해도 좋음

![이미지3](/image/common_image_03.PNG)<br>

```c

# ROS_MASTER_URI
# ROS_HOSTNAME
# 을 수정한다.
$ sudo gedit ~/.bashrc

```

![이미지4](/image/common_image_04.PNG)<br>

```c

# 수정이후 다음 커맨드
$ source ~/.bashrc

```

---

[참고사이트](http://emanual.robotis.com/docs/en/platform/turtlebot3/pc_setup/)<br>

---

[Top](#Main)<br>

# 01.2라즈베리파이세팅

두 가지 방법이 있는데

1. Ubuntu MATE
2. Raspbian

우선은 Ubuntu MATE로 설치를 해본다.

> (참고) Ubuntu MATE도 용량이 크기에 최소 16GB이상이여야 함.

---

Ubuntu MATE : 16.04.5(Xenial)

[다운로드 사이트](https://ubuntu-mate.org/download/)<br>

> (참고로 NAS 서버에도 있음)

---

Turtlebot3 PC에서 SBC의 Ubuntu MATE를 다음과 같이 올린다.

참고로 나는 Turtlebot3 PC가 VirtualBox에 설치해서 설정이 좀 더 들어갔음.(상세설정은 쉽기에 생략)

![이미지](/image/SBC_Setting_image1.png)<br>

![이미지2](/image/SBC_Setting_image2.png)<br>

목록 -> 디스크 이미지 복구 선택

안될경우는 이미지파일 손상의 확률이 크기에 이미지파일을 다시 다운 받는 것을 추천

위가 안될경우(VirtualBox는 안됨) [여기](https://yeopbox.com/%EB%9D%BC%EC%A6%88%EB%B2%A0%EB%A6%AC%ED%8C%8C%EC%9D%B4-3-raspberry-pi%EC%97%90-%EC%9A%B0%EB%B6%84%ED%88%AC-%EB%A9%94%EC%9D%B4%ED%8A%B8-16-04-lts-%EC%84%A4%EC%B9%98%ED%95%98%EA%B8%B0/)<br>를 참고

차이점은 SDCard 포매터로 포맷 이후 Win32 Disk Imager로 Ubuntu MATE를 설치한다는 점.

[SDCard 포매터](https://www.sdcard.org/downloads/formatter_4/)<br>
[Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/files/latest/download)<br>

`결론은 윈도우에서 SDCard Formatter + Win32 Disk Imager 조합이 나음 리눅스보다`

---

[SBC]

> 설치가 오래걸리기에 시간을 넉넉히 준비해야한다.

라즈베리파이에 HDMI케이블, 마우스, 키보드 연결하여 Ubuntu MATE 설치

설치는 일반 우분투 리눅스 설치와 동일하기에 생략.

```c

# 설치완료 후 아래 명령어 입력
$ sudo apt-get update
$ sudo apt-get upgrade
$ wget https://raw.githubusercontent.com/ROBOTIS-GIT/robotis_tools/master/install_ros_kinetic_rp3.sh && chmod 755 ./install_ros_kinetic_rp3.sh && bash ./install_ros_kinetic_rp3.sh

```

```c

# 필요한 SW 클론
$ git clone https://github.com/ROBOTIS-GIT/hls_lfcd_lds_driver.git
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3.git

```

```c

# 필요없는 SW제거
$ cd ~/catkin_ws/src/turtlebot3
$ sudo rm -r turtlebot3_description/ turtlebot3_teleop/ turtlebot3_navigation/ turtlebot3_slam/ turtlebot3_example/

```

```c

# 필요한 패키지 설치
$ sudo apt-get install ros-kinetic-rosserial-python ros-kinetic-tf

```

```c

# USB 세팅
$ rosrun turtlebot3_bringup create_udev_rules

```

아이피 주소 세팅

![이미지3](/image/SBC_Setting_image3.PNG)

```c

$ sudo gedit ~/.bashrc

```

![이미지4](/image/SBC_Setting_image4.PNG)

```c

$ source ~/.bashrc

```
---

## PC에서 SBC로 SSH로 접근하기

SBC에 SSH를 설치해야함.

```c

$ dpkg --get-seletions | grep ssh
# ssh 설치여부를 출력

```

```c

$ service ssh status
# 현재 ssh가 실행중인지 여부를 출력

```

```c

$ service ssh restart
# 설치 후 재시작

```

```c

# 설치를 했는대 inactive라면
$ sudo nano /etc/rc.local

# exit 0 위에 다음을 추가한다.
$ /bin/sh -e /etc/init.d/ssh start

```

[참고사이트1](https://zetawiki.com/wiki/%EC%9A%B0%EB%B6%84%ED%88%AC_sshd_%EC%84%A4%EC%B9%98)<br>
[참고사이트2](http://ocworld.net/?p=2304)<br>

---

[참고사이트](http://emanual.robotis.com/docs/en/platform/turtlebot3/sbc_setup/)<br>


---

[Top](#Main)<br>

# 01.3OpenCR세팅

---

[개발환경 구축 홈페이지](http://emanual.robotis.com/docs/en/platform/turtlebot3/setup/#setup)<br>

직접세팅해봄

1. [PC 세팅](markdown/turtlebot3_PC_Setting)
2. [라즈베리파이 세팅](markdown/turtlebot3_SBC_Setting)
3. [OpenCR 세팅]()

---

[Top](#Main)<br>

# 02.Bringup

```c

$ roscore

```

```c

$ roslaunch turtlebot3_bringup turtlebot3_robot.launch

```

```c

$ rosluanch turtlebot3_bringup turtlebot3_remote.launch
$ rosrun rviz rviz -d 'rospack find turtlebot3_description' /rviz/model.rviz

```

`주의) Virtual Machine에서 rviz를 사용하기 위해서는 3D 가속화를 끄고 실행해야함.`

---

[Top](#Main)<br>

# 02.1_Topic_Monitor_실행

[Topic Monitor 메뉴얼](http://emanual.robotis.com/docs/en/platform/turtlebot3/topic_monitor/)<br>

```c

# Remote PC
$ roscore

```

```c

# Turtlebot3 SBC
# puttry로 접속 id : turtlebot3sbc / pw : hyung9150@
$ roslaunch turtlebot3_bringup turtlebot3_robot.launch

```

```c

# Remote PC
$ rqt

```

---

[Top](#Main)<br>

# ROS_launch_설명

## 정의 :

rosrun이 하나의 노드를 실행하는 명령어라면, roslaunch는 복수의 노드를 실행하는 개념

roslaunch는 *.launch라는 로스런치파일을 사용하여 실행 노드에 대한 설정을 해준다.

*.launch는 xml 기반으로 되어 있으며 태그별 옵션을 제공한다.

```c

// ROS Launch Example
<launch>
  <arg name="multi_robot_name" default=""/>

  <node pkg="rosserial_python" type="serial_node.py" name="turtlebot3_core" output="screen">
    <param name="port" value="/dev/ttyACM0"/>
    <param name="baud" value="115200"/>
    <param name="tf_prefix" value="$(arg multi_robot_name)"/>
  </node>
</launch>


```

---

## 만들어 보자.


```c

$ cd ~/catkin_ws/src/oroca_ros_tutorials
$ mkdir launch
$ cd launch
$ gedit union.launch

```

```c

<launch>
  <arg name="multi_robot_name" default=""/>

  <node pkg="rosserial_python" type="serial_node.py" name="turtlebot3_core" output="screen">
    <param name="port" value="/dev/ttyACM0"/>
    <param name="baud" value="115200"/>
    <param name="tf_prefix" value="$(arg multi_robot_name)"/>
  </node>
</launch>

```

`launch` 는 로스런치 태그로써 이 태그안에는 로스런치에 필요한 태그들이 기술된다.

`node` 는 로스런치로 실행할 노드를 기술하게 된다. 옵션으로는 pkg, type, name 이 있다. pkg는 패키지의 이름, type는 실제 실행할 노드의 이름, name은 type를 실행하되 실행할때 붙여지는 이름이다.  

## 실행해 보자.

```c

$ roslaunch oroca_ros_tutorials union.launch

```

```c

$ rosnode list

```

```c

/msg_publisher1
/msg_publisher2
/msg_subscriber1
/msg_subscriber2
/rosout

```

---

## 그룹이란 옵션을 사용해보자.

```c

<launch>

  <group ns="ns1">
    <node pkg="oroca_ros_tutorials" type="ros_tutorial_msg_publisher"   name="msg_publisher"/>
    <node pkg="oroca_ros_tutorials" type="ros_tutorial_msg_subscriber"  name="msg_subscriber"/>
  </group>

  <group ns="ns2">
    <node pkg="oroca_ros_tutorials" type="ros_tutorial_msg_publisher"  name="msg_publisher"/>
    <node pkg="oroca_ros_tutorials" type="ros_tutorial_msg_subscriber"  name="msg_subscriber"/>
  </group>

</launch>

```

---

## 기타 roslauch 파라미터

`launch` 로스런치 구문의 시작과 끝을 가르킨다.

`node` 노드 실행에 대한 태그이다. 패키지, 노드명, 실행명을 변경할 수 있다.

`machine` 노드를 실행하는 PC의 이름, address,  ros-root,  ros-package-path 등을 설정할 수 있다.

`include` 다른 패키지 및 같은 패키지에 속해있는 다른 로스런치를 불러와 하나의 파일처럼 실행 시킬 수 있다.

`remap` 토픽 이름 등의 노드에서 사용중인 로스변수의 이름을 변경할 수 있다.

`env` 환경 변수를 변경한다.

`param` 로스 매개변수를 변경한다

`rosparam` 로스런치에서 rosparam 의 명령어를 이용하는 태그이다.

`group` 실행되는 노드를 그룹화할때 사용되는 태그이다.

`test` 노드를 테스트할 때 사용되는 태그로 node 와 비슷하지만 테스트에 사용되는 옵션들이 추가되어 있다.

`arg` 로스런치에서 사용되는 변수를 정의할 수 있고, 로스런치에서 변수처럼 재사용되어 주소 및 대체 이름등에 사용된다.

---

[참고사이트](https://cafe.naver.com/openrt/3113)<br>

---

[Top](#Main)<br>

# turtlebot3_robot.launch

```c

<launch>
  <arg name="multi_robot_name" default=""/>
  <arg name="set_lidar_frame_id" default="base_scan"/>

  <include file="$(find turtlebot3_bringup)/launch/turtlebot3_core.launch">
    <arg name="multi_robot_name" value="$(arg multi_robot_name)"/>
  </include>
  <include file="$(find turtlebot3_bringup)/launch/turtlebot3_lidar.launch">
    <arg name="set_frame_id" value="$(arg set_lidar_frame_id)"/>
  </include>

  <node pkg="turtlebot3_bringup" type="turtlebot3_diagnostics" name="turtlebot3_diagnostics" output="screen"/>
</launch>

```

---

[Top](#Main)<br>

# turtlebot3_core.launch


```c

<launch>
  <arg name="multi_robot_name" default=""/>

  <node pkg="rosserial_python" type="serial_node.py" name="turtlebot3_core" output="screen">
    <param name="port" value="/dev/ttyACM0"/>
    <param name="baud" value="115200"/>
    <param name="tf_prefix" value="$(arg multi_robot_name)"/>
  </node>
</launch>

```

---

[Top](#Main)<br>

# turtlebot3_lidar.launch

```c

<launch>
  <arg name="set_frame_id" default="base_scan"/>

  <node pkg="hls_lfcd_lds_driver" type="hlds_laser_publisher" name="turtlebot3_lds" output="screen">
    <param name="port" value="/dev/ttyUSB0"/>
    <param name="frame_id" value="$(arg set_frame_id)"/>
  </node>
</launch>

```

---

[Top](#Main)<br>

# 03.1SLAM

```c

# Remote PC Terminal 1

$ roscore

```

```c

# TurtleBot

$ roslaunch turtlebot3_bringup turtlebot3_robot.launch

```

```c

# Remote PC Terminal 2

$ export TURTLEBOT3_MODEL=${burger}
$ roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gampping

```

---

[Top](#Main)<br>

# rosserial_python

python을 기반으로 만들어진 패키지

rosrun으로 실행

```c

$ rosrun rosserial_python serial_node.py _port:=/dev/ttyACM1 _baud:=115200

```

roslaunch으로 실행

```c

<launch>
  <node pkg="rosserial_python" type="serial_node.py" name="serial_node">
    <param name="port" value="/dev/ttyACM1"/>
    <param name="baud" value="115200"/>
  </node>
</launch>

```

---

[참고사이트](http://wiki.ros.org/rosserial_python)<br>